package com.explore.venus.service.impl;

import com.explore.venus.service.RemoteConsumerService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * @author hisokey
 * @date 2022/10/22
 * @apiNote
 */
@Service
public class RemoteConsumerFallbackServiceImpl implements RemoteConsumerService {
    @Override
    public String getInfo() {
        return "[ 远程调用失败 venus-provider ] : 当前系统的时间为 -> " + LocalDateTime.now();
    }
}
