package com.explore.venus.service;

import com.explore.venus.service.impl.RemoteProviderFallbackServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author hisokey
 * @date 2022/10/22
 * @apiNote
 */
@Service
@FeignClient(value = "venus-provider", fallback = RemoteProviderFallbackServiceImpl.class)
public interface RemoteProviderService {

    @GetMapping("/provider/show_info")
    public String getInfo();
}
