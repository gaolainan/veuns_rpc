package com.explore.venus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author hisokey
 * @date 2022/10/22
 * @apiNote
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class VenusConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(VenusConsumerApplication.class, args);
    }
}
