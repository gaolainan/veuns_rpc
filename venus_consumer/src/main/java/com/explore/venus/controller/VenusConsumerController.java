package com.explore.venus.controller;

import com.explore.venus.service.RemoteProviderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * @author hisokey
 * @date 2022/10/22
 * @apiNote
 */
@RestController
@RequestMapping("/consumer")
public class VenusConsumerController {

    @Resource
    private RemoteProviderService remoteProviderService;

    @GetMapping("/get_time")
    public String getProviderServiceInfo(){
        return remoteProviderService.getInfo();
    }

    @GetMapping("/show_info")
    public String showConsumerApplicationInfo(){
        return "[ 远程调用成功 venus-consumer ] : 当前系统的时间为 -> " + LocalDateTime.now();
    }
}
