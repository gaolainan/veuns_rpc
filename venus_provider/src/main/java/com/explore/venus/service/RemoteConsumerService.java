package com.explore.venus.service;

import com.explore.venus.service.impl.RemoteConsumerFallbackServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author hisokey
 * @date 2022/10/22
 * @apiNote
 */
@Service
@FeignClient(value = "venus-consumer", fallback = RemoteConsumerFallbackServiceImpl.class)
public interface RemoteConsumerService {


    @GetMapping("/consumer/show_info")
    public String getInfo();
}
