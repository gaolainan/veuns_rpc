package com.explore.venus;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

/**
 * @author hisokey
 * @date 2022/10/22
 * @apiNote
 */
@Service
@Slf4j
public class Boot implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("--- Boot started ---");
        log.info("--- Boot ended ---");
    }
}
