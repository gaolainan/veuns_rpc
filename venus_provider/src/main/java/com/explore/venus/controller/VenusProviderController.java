package com.explore.venus.controller;

import com.explore.venus.service.RemoteConsumerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * @author hisokey
 * @date 2022/10/22
 * @apiNote
 */
@RestController
@RequestMapping("/provider")
public class VenusProviderController {

    @Resource
    private RemoteConsumerService remoteConsumerService;

    @GetMapping("/get_time")
    public String getConsumerApplicationInfo(){
        return remoteConsumerService.getInfo();
    }

    @GetMapping("/show_info")
    public String showProviderApplicationInfo(){
        return "[ 远程调用成功 venus-provider ] : 当前系统的时间为 -> " + LocalDateTime.now();
    }
}
